package com.folcademy.bancofels.models.dto;

public class UserDTO {
    String firstName;
    String lastName;
    String cbu;

    public UserDTO() {
    }

    public UserDTO(String firstName, String lastName, String cbu) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cbu = cbu;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCbu() {
        return cbu;
    }
}
