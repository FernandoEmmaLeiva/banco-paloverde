package com.folcademy.bancofels.models.dto;

import com.folcademy.bancofels.models.enums.Currency;
import java.math.BigDecimal;

public class TransactionDetailDTO {
    private String date;
    private String description;
    private BigDecimal amount;
    private Currency currency;
    private UserDTO from;
    private UserDTO to;

    public TransactionDetailDTO() {
    }

    public TransactionDetailDTO(String date, String description, BigDecimal amount, Currency currency, UserDTO from, UserDTO to) {
        this.date = date;
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = from;
        this.to = to;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public UserDTO getFrom() {
        return from;
    }

    public UserDTO getTo() {
        return to;
    }
}