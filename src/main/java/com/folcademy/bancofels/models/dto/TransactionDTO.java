package com.folcademy.bancofels.models.dto;

import com.folcademy.bancofels.models.enums.Currency;

import java.math.BigDecimal;

public class TransactionDTO {
    private String date;
    private String description;
    private BigDecimal amount;
    private Currency currency;
    private String from;
    private String to;
    private String type;

    public TransactionDTO() {
    }

    public TransactionDTO(String date, String description, BigDecimal amount, Currency currency, String from, String destination, String type) {
        this.date = date;
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = from;
        this.to = destination;
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getType() {
        return type;
    }

}
