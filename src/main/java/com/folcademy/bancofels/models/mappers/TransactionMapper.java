package com.folcademy.bancofels.models.mappers;

import com.folcademy.bancofels.models.dto.TransactionDTO;
import com.folcademy.bancofels.models.entities.TransactionsEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TransactionMapper {

    public TransactionsEntity mapTransactionDTOtoTransactionEntity(TransactionDTO newTransactionDTO){
        return new TransactionsEntity(
            new Date(),
                newTransactionDTO.getDescription(),
                newTransactionDTO.getAmount(),
                newTransactionDTO.getCurrency(),
                newTransactionDTO.getFrom(),
                newTransactionDTO.getTo()
        );
    }
}
