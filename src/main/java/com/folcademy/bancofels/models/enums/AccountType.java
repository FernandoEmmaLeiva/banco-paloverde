package com.folcademy.bancofels.models.enums;

public enum AccountType {
  CUENTA_CORRIENTE, CAJA_AHORRO
}
