package com.folcademy.bancofels.models.respositories;

import com.folcademy.bancofels.models.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<UserEntity, String>
{
    UserEntity findByDni(String dni);
}
