package com.folcademy.bancofels.models.respositories;

import com.folcademy.bancofels.models.entities.AccountsEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountsRespositories extends CrudRepository<AccountsEntity, Long> {
    List<AccountsEntity> findAllByUserId(String UserId);
    AccountsEntity findByNumber(Long accountNumber);
}
