package com.folcademy.bancofels.models.respositories;

import com.folcademy.bancofels.models.entities.TransactionsEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionRepository extends CrudRepository<TransactionsEntity, Long> {

    public List<TransactionsEntity> findAllByOriginOrDestination(String origin, String destination);
    public TransactionsEntity findByTransactionNumber(Long transactionNumber);
}
