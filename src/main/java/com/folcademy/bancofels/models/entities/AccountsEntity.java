package com.folcademy.bancofels.models.entities;

import com.folcademy.bancofels.models.enums.AccountType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;


@Entity(name = "accounts")
public class AccountsEntity {
    @Id
    private long number;
    private String cbu;

    @Enumerated(EnumType.STRING)
    private AccountType type;

    private String userId;

    public AccountsEntity() {
    }

    public AccountsEntity(long number, String cbu, AccountType type, String userId) {
        this.number = number;
        this.cbu = cbu;
        this.type = type;
        this.userId = userId;
    }

    public String getCbu() {
        return cbu;
    }

    public long getNumber() {
        return number;
    }

    public AccountType getType() {
        return type;
    }

    public String getUserId() {
        return userId;
    }
}
