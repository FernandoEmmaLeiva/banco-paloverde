package com.folcademy.bancofels.models.entities;

import com.folcademy.bancofels.models.enums.Currency;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "transactions")
public class TransactionsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long transactionNumber;
    private Date date;
    private String description;
    private BigDecimal amount;
    private Currency concurrency;
    private String origin;
    private String destination;

    public TransactionsEntity() {
    }

    public TransactionsEntity(Date date, String description, BigDecimal amount, Currency concurrency, String origin, String destination) {
        this.date = date;
        this.description = description;
        this.amount = amount;
        this.concurrency = concurrency;
        this.origin = origin;
        this.destination = destination;
    }

    public Long getTransactionNumber() {
        return transactionNumber;
    }

    public String getDestination() {
        return destination;
    }

    public Date getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getConcurrency() {
        return concurrency;
    }

    public String getOrigin() {
        return origin;
    }

    public boolean isValid(){
        return this.origin.compareTo(this.destination) != 0 && this.amount.compareTo(BigDecimal.ZERO) == 1;
    }
}
