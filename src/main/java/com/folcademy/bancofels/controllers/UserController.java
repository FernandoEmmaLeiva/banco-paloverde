package com.folcademy.bancofels.controllers;

import com.folcademy.bancofels.models.entities.UserEntity;
import com.folcademy.bancofels.models.respositories.UserRepository;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserRepository userRepository;

    public UserController(UserRepository userRepository){
        this.userRepository =  userRepository;
    }

    @GetMapping("/{dni}")
    public Optional<UserEntity> getUser(@PathVariable String dni){
        return userRepository.findById(dni);
    }

    @PostMapping("/new")
    public UserEntity newUser(@RequestBody UserEntity userEntity){
        return userRepository.save(userEntity);
    }
}
