package com.folcademy.bancofels.controllers;

import com.folcademy.bancofels.models.dto.TransactionDTO;
import com.folcademy.bancofels.models.dto.TransactionDetailDTO;
import com.folcademy.bancofels.models.dto.TransactionsDTO;
import com.folcademy.bancofels.services.TransactionsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/transactions")
public class TransactionsController {
    private final TransactionsService transactionsService;

    public TransactionsController(TransactionsService transactionsService){
        this.transactionsService = transactionsService;
    }

    @GetMapping("/all/{accountNumber}")
    public ResponseEntity<TransactionsDTO> getTrasactionsForAccount(@PathVariable Long accountNumber){
        return new ResponseEntity<>(transactionsService.getTransactions(accountNumber), HttpStatus.OK);
    }

    @GetMapping("{transactionId}")
    public ResponseEntity<TransactionDetailDTO> getTransactions(@PathVariable Long transactionId){
        return new ResponseEntity<>(transactionsService.getTransaction(transactionId), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<String> createTransaction(@RequestBody TransactionDTO newTransactionDTO){
        return transactionsService.createTransaction(newTransactionDTO);
    }
}
