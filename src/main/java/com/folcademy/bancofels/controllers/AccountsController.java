package com.folcademy.bancofels.controllers;

import com.folcademy.bancofels.models.entities.AccountsEntity;
import com.folcademy.bancofels.models.respositories.AccountsRespositories;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountsController {
    private final  AccountsRespositories  accountsRespository;

    public AccountsController(AccountsRespositories accountsRespository){
        this.accountsRespository = accountsRespository;
    }

    @GetMapping("/{userId}")
    public List<AccountsEntity> getUserAccounts(@PathVariable String userId){
        return accountsRespository.findAllByUserId(userId);
    }

    @PostMapping("/new")
    public AccountsEntity newAccount(@RequestBody AccountsEntity accountEntity){
        return accountsRespository.save(accountEntity);
    }
}
