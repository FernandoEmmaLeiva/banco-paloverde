package com.folcademy.bancofels.services;

import com.folcademy.bancofels.models.dto.TransactionDTO;
import com.folcademy.bancofels.models.dto.TransactionDetailDTO;
import com.folcademy.bancofels.models.dto.TransactionsDTO;
import com.folcademy.bancofels.models.dto.UserDTO;
import com.folcademy.bancofels.models.entities.AccountsEntity;
import com.folcademy.bancofels.models.entities.TransactionsEntity;
import com.folcademy.bancofels.models.entities.UserEntity;
import com.folcademy.bancofels.models.mappers.TransactionMapper;
import com.folcademy.bancofels.models.respositories.AccountsRespositories;
import com.folcademy.bancofels.models.respositories.TransactionRepository;
import com.folcademy.bancofels.models.respositories.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionsService {

    private final TransactionRepository transactionsRepository;
    private final AccountsRespositories accountRepository;
    private final UserRepository userRepository;
    private final TransactionMapper transactionMapper;

    public TransactionsService(TransactionRepository transactionsRepository, AccountsRespositories accountRepository, UserRepository userRepository, TransactionMapper transactionMapper) {
        this.transactionsRepository = transactionsRepository;
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.transactionMapper = transactionMapper;
    }

    public TransactionsDTO getTransactions(Long accountNumber){
        List<TransactionsEntity> transactionsEntities = transactionsRepository.findAllByOriginOrDestination(accountNumber.toString(), accountNumber.toString());
        List<TransactionDTO> transactionDTOList = new ArrayList<>();

        for(TransactionsEntity transactionsEntity: transactionsEntities){
            String transactionType;
            if(transactionsEntity.getOrigin().equals(accountNumber.toString()))
            {
                transactionType = "GASTO";
            }else
            {
                transactionType = "INGRESO";
            }

            transactionDTOList.add(
                    new TransactionDTO(transactionsEntity.getDate().toString(),
                            transactionsEntity.getDescription(),
                            transactionsEntity.getAmount(),
                            transactionsEntity.getConcurrency(),
                            transactionsEntity.getOrigin(),
                            transactionsEntity.getDestination(),
                            transactionType)
            );
        }

        BigDecimal balance = BigDecimal.ZERO;
        for (TransactionDTO transactionDTO: transactionDTOList) {
            if(transactionDTO.getType().equals("GASTO")){
                balance = balance.subtract(transactionDTO.getAmount());
            }
            else{
                balance = balance.add(transactionDTO.getAmount());
            }
        }

        return new TransactionsDTO(transactionDTOList, balance);
    }

    public TransactionDetailDTO getTransaction(Long transactionId){

       TransactionsEntity transactionsEntity = transactionsRepository.findByTransactionNumber(transactionId);
       AccountsEntity accountTo = accountRepository.findByNumber(Long.parseLong(transactionsEntity.getOrigin()));
       UserEntity userEntityTo = userRepository.findByDni(accountTo.getUserId());
       AccountsEntity accountFrom = accountRepository.findByNumber(Long.parseLong(transactionsEntity.getDestination()));
       UserEntity userEntityFrom = userRepository.findByDni(accountFrom.getUserId());
       if(transactionsEntity != null){
           return new TransactionDetailDTO(
                   transactionsEntity.getDate().toString(),
                   transactionsEntity.getDescription(),
                   transactionsEntity.getAmount(),
                   transactionsEntity.getConcurrency(),
                   new UserDTO(userEntityTo.getFirtName(), userEntityTo.getLastName(), accountTo.getCbu()),
                   new UserDTO(userEntityFrom.getFirtName(), userEntityFrom.getLastName(), accountFrom.getCbu())
           );
       }else{
           return new TransactionDetailDTO();
       }

    }

    public ResponseEntity<String> createTransaction(TransactionDTO newTransactionDTO){
        try {
            TransactionsEntity transactionsEntity =  transactionMapper.mapTransactionDTOtoTransactionEntity(newTransactionDTO);
            AccountsEntity accountsEntity = accountRepository.findByNumber(Long.parseLong(transactionsEntity.getDestination()));
            if(accountsEntity != null && transactionsEntity.isValid()) {
                transactionsRepository.save(transactionsEntity);
                return new ResponseEntity<>("Transacción creada", HttpStatus.OK);
            }else
                return new ResponseEntity<>("Error al crear la transacción", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Error Tremendo", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
